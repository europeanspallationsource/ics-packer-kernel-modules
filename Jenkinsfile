pipeline {
    agent { label 'packer' }
    environment {
        GPG_PASSWORD = credentials('ics-gpg-password')
    }

    stages {
        stage('Validate') {
            steps {
                slackSend (color: 'good', message: "STARTED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
                sh 'packer validate modules.json'
            }
        }
        stage('Build') {
            steps {
                ansiColor('xterm') {
                    sh 'packer build modules.json'
                }
            }
        }
        stage('Upload to artifactory') {
            steps {
                script {
                    def server = Artifactory.server 'artifactory.esss.lu.se'
                    def uploadSpec = """{
                      "files": [
                        {
                          "pattern": "RPMS/*.rpm",
                          "target": "rpm-ics/centos/7/x86_64/"
                        }
                      ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }
    }

    post {
        always {
            /* clean up the workspace */
            deleteDir()
        }
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }

}
