Packer template to build kernel modules
=======================================

This Packer_ template builds kernel modules using a VirtualBox machine
with RT kernel.

Modules built:

- ethercat


Usage
-----

The RPMs are built automatically and uploaded to artifactory by Jenkins
(you can check the Jenkinsfile).

To build the RPMs locally, run::

    $ packer build modules.json

This will create the RPMs under RPMS/


.. _Packer: https://www.packer.io
