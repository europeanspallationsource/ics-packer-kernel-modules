%define module_name ethercat
%define version     1.5.2
%define release     ESS3
%define repo_name   m-ethercat
%define kversion    %(uname -r)
%define buildroot %{_topdir}/%{name}-%{version}-root
%define _unpackaged_files_terminate_build 0

BuildRoot:  %{buildroot}
Summary:        %{module_name} kernel module
License:        GPL
Name:           kmod-%{module_name}
Version:        %{version}
Release:        %{release}
Url:            https://bitbucket.org/europeanspallationsource/m-ethercat

%description
%{module_name} kernel module

%prep
rm -rf %{repo_name}
git clone %{url} %{repo_name}
cd %{repo_name}

%build
cd %{repo_name}/ethercat-1.5.2
autoreconf
./configure --disable-8139too --enable-generic
make modules

%install
make -C %{repo_name}/ethercat-1.5.2 modules_install INSTALL_MOD_PATH=$RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}/etc/udev/rules.d
echo 'KERNEL=="EtherCAT[0-9]*", MODE="0666"' > ${RPM_BUILD_ROOT}/etc/udev/rules.d/90-ethercat.rules

%post
/sbin/depmod -a

%postun
/sbin/depmod -a

%files
%defattr(-,root,root)
/lib/modules/%{kversion}/%{module_name}/master/ec_master.ko
/lib/modules/%{kversion}/%{module_name}/devices/ec_generic.ko
/lib/modules/%{kversion}/%{module_name}/examples/mini/ec_mini.ko
/etc/udev/rules.d/90-ethercat.rules
